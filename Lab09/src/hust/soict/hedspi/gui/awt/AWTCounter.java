package hust.soict.hedspi.gui.awt;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

 public class AWTCounter extends Frame implements ActionListener{
//khai bao cac control dieu khien
	private Label lblCount;
	private TextField tfCount;
	private Button btnCount;
	private int count;//bien chua gia tri dem
	
	//constuc
	public AWTCounter() {
		//thiet lap layout
		this.setLayout(new FlowLayout());
		//khoi toa control
		lblCount = new Label("Counter");
				//them vao giao dien
				this.add(lblCount);
		
		tfCount = new TextField(count + "",10);
		tfCount.setEditable(false);
		this.add(tfCount);
		btnCount = new Button("Count");
		this.add(btnCount);
		//dang ki lang nghe su kien
		btnCount.addActionListener(this);
		
		//thiet lap thong tin
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		
		
	    this.setTitle("AWT Counter");
	    this.setSize(250,100);
	    this.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        AWTCounter app = new AWTCounter();
	}
  public void actionPerformed(ActionEvent e) {
	  ++count;
	  this.tfCount.setText(count + "");  //chuyen count thanh string
  }
  
 
}
