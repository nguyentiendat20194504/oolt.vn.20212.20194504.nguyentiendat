package hust.soict.hedspi.aims.order;

import java.util.ArrayList;
import java.util.Random;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.utils.MyDate;

public class Order {
    // Indicate the maximum number of items that a customer can buy
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERED = 5;
    public static final int nbOrder = 0;
    // Add a field as an array to store list of Media
    public ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    // Create a qtyOrdered
    private int qtyOrdered = 0;
    private MyDate dateOrdered;
    private static int nbOrders = 0;

    public Order() {
        if (Order.nbOrders == MAX_LIMITED_ORDERED) {
            System.out.println("Over numbers of order");
        } else {
            this.qtyOrdered = 0;
            this.dateOrdered = new MyDate();
            nbOrders++;
        }
    }

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    // Create method
    public void print() {
		System.out.println("*************************Order*************************");
		dateOrdered.print();
		System.out.println("Ordered Items:");
		int i = 1;
		System.out.printf("%-3s | %-6s | %-20s | %-15s |  %-5s ", "STT", "ID", "Title", "Category", "Total");
		System.out.println();
		for(Media media: itemsOrdered) {
				System.out.printf("%-3s | %-6s | %-20s | %-15s | %.2f$\n", i, media.getId(), media.getTitle(), media.getCategory(), media.getCost());
			i++;
		}
		System.out.println("Total cost: " + this.totalCost() + "$");
		System.out.println("*******************************************************");
	}

    // Add Media
    public void addMedia(Media media) {
        if (this.itemsOrdered.contains(media)) {
            System.err.println("ERR: The media with title: " + media.getTitle() + " is existed!");
        } else {
            this.itemsOrdered.add(media);
            System.out.println("MSG: The media with title: " + media.getTitle() + " has been added.");
        }
    }

    public void addMedia(Media[] mediaList) {
        if (this.qtyOrdered == MAX_NUMBERS_ORDERED || this.qtyOrdered + mediaList.length > MAX_NUMBERS_ORDERED) {
            System.out.println("The order is full.");
        } else {
            for (int i = 0; i < mediaList.length; i++) {
                this.addMedia(mediaList[i]);
            }
        }
    }

    public void addMedia(Media media1, DigitalVideoDisc media2) {
        this.addMedia(media1);
        this.addMedia(media1);
    }

    public boolean removeMedia(String id) {
		boolean tmp = false;
		for(Media media: itemsOrdered) {
			if(media.getId().equalsIgnoreCase(id)) {
				removeMedia(media);
				tmp = true;
				break;
			}
		}
		if(tmp == false) {
			System.err.println("ERR: Id isn't founded!");
		}
		return tmp;
	}
	
	public void removeMedia(Media item) {
		if(itemsOrdered.contains(item)) {
			System.out.println("MSG: Media with ID: " + item.getId() + " has been deleted!");
			itemsOrdered.remove(item);
		}else {
			System.err.println("ERR: The media may not exist!");
		}
	}

    // Sum of costs
    public float totalCost() {
        float total = 0.0f;
        Media mediaItem;
        java.util.Iterator iter = itemsOrdered.iterator();
        while (iter.hasNext()){
            mediaItem = (Media) (iter.next());
            total += mediaItem.getCost();
        }
        return total;
    }

    // Xay dung phuong thuc chon mot san pham ngau nhien trong danh sach mien phi
    public Media getALuckyItem() {
        Random rd = new Random();
        int luckyNumber = rd.nextInt(this.itemsOrdered.size());
        itemsOrdered.get(luckyNumber).setCost(0);
        return this.itemsOrdered.get(luckyNumber);
    }

}
